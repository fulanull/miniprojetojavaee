package br.com.project.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User {

	@Id
	@GeneratedValue
	private long id;

	@Column(unique = true, length = 30)
	private String name;

	@Column(unique = false, nullable = false, length = 35)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USER_AUTHORITY", joinColumns = @JoinColumn(name = "USER_ID"), inverseJoinColumns = @JoinColumn(name = "AUTHORITY_ID"))
	private List<Authority> roles;

	public User() {
		this.roles = new ArrayList<Authority>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Authority> getRoles() {
		return roles;
	}

	public void setRoles(List<Authority> roles) {
		this.roles = roles;
	}

	public void imprimir() {
		System.out.println("Username: " + this.getName());
		System.out.println("Password: " + this.getPassword());
	}

	public boolean hasRole(String role) {
		if (this.roles == null) {
			return false;
		} else {
			for (Authority au : this.roles) {
				if(role.equals(au.getRole())){
					return true;
				}
			}
		}

		return false;
	}

}
