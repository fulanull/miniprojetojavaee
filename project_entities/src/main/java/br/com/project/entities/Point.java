package br.com.project.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="_point")
public class Point {
	@Id 
	@GeneratedValue
	private long id;
	
	private String title;
	private Date x;
	private double h;
	private double i;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getX() {
		return x;
	}
	public void setX(Date x) {
		this.x = x;
	}
	public double getH() {
		return h;
	}
	public void setH(double h) {
		this.h = h;
	}
	public double getI() {
		return i;
	}
	public void setI(double i) {
		this.i = i;
	}
	
	
	
}
