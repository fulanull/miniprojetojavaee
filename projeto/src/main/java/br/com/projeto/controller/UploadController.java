package br.com.projeto.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import br.com.project.service.impl.FileUploadBOImpl;

//@ManagedBean(name = "uploadController")
@Controller("uploadController")

@PreAuthorize("hasRole('ROLE_ADM')")
public class UploadController {

	private UploadedFile file;
	private String name = "Leu Bean";
	
	@Resource
	private FileUploadBOImpl fileUpload;

	public UploadController() {
		System.out.println("UploadController: iniciado!");
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {

		if (file != null) {
			System.out.println("UploadController: setFile " + file.getFileName() + " !");
		} else {
			System.out.println("UploadController: setFile!");
		}
		this.file = file;
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
		System.out.println("UploadController: destru�do!");
	}

	public void upload() {
		System.out.println("UploadController: upload!");
		try {
			if (this.file!=null)
			{
				this.fileUpload.addData(file.getInputstream());
				FacesMessage message = new FacesMessage("Succesful", this.file.getFileName() + " is uploaded.");
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Error");
			e.printStackTrace();
		}finally{
			this.file = null;
		}

	}

	public void handleFileUpload(FileUploadEvent event) {
		System.out.println("handleFileUpload: upload!");
		FacesMessage message;
		this.file = event.getFile();
		try {
			this.fileUpload.addData(file.getInputstream());
			message = new FacesMessage("Succesful", event.getFile().getFileName() + " was uploaded.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Error");
			message = new FacesMessage("Error", event.getFile().getFileName() + " Was NOT uploaded.");
			e.printStackTrace();
		}
			
		
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

}
