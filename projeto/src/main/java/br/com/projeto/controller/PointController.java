package br.com.projeto.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.faces.event.ValueChangeEvent;

import org.springframework.stereotype.Controller;
import org.primefaces.context.RequestContext;

import br.com.project.entities.Point;
import br.com.project.service.PointBO;

@Controller("pointController")
public class PointController {

	@Resource
	private PointBO pointBO;



	private List<Point> points = null;
	private List<String> titles = null;
	private String dados = null;

	public PointController(){
		System.out.println("PointController criado");
	}

	public List<Point> getPoints() {
		System.out.println("PointController: getPoints");
		//
		//		if (points == null) {
		//			this.points = this.pointBO.getAll();
		//		}
		//		return this.points;
		return this.pointBO.getAll();
	}

	public List<Point> getPointbyTitle(String title){
		return this.pointBO.getByTitle(title);		
	}

	public List<String> getTitles() {
		return this.pointBO.getTitles();
	}

	//m�todo de change do combo
	//chamar seu servi�o de parser
	//system na string aqui
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		System.out.println("PointController destru�do");
	}

	public void valueChangeTitleCB(ValueChangeEvent e){
		if (e!=null)
		{
			String title = e.getNewValue().toString();
			String data = this.pointBO.getDataByTitle(title);
			this. setDados(data);

			//chamar JS
			RequestContext.getCurrentInstance().execute("createGraphicLine('"+data+"','"+title+"')");
			System.out.println("Pontos: \n" + this.getDados());
		}
	}

	public String getDados() {
		return dados;
	}

	public void setDados(String dados) {
		this.dados = dados;
	}



}
