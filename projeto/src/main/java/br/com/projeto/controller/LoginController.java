package br.com.projeto.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.bean.RequestScoped;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;

import br.com.project.entities.Authority;
import br.com.project.entities.User;
import br.com.project.service.LoginBO;

@Controller("loginController")
public class LoginController implements AuthenticationProvider {
	
	@Resource
	LoginBO loginBO;

	public LoginController() {
		System.out.println("LoginController: Criado!");
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
		System.out.println("LoginController: Destru�do!");
	}




	public User getUser() throws Exception {			
		return this.loginBO.getLogedUser();
	}

	// --------------------------------
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		// TODO Auto-generated method stub
		User user;

		try {
			String name = authentication.getName();
			String password = authentication.getCredentials().toString();

			List<GrantedAuthority> grantedAuths = new ArrayList<>();
			Authentication auth;

			try {
				user = this.loginBO.login(name, password);
			} catch (Exception ex) {
				throw new AuthenticationCredentialsNotFoundException(
						"Error in authenticate", ex);
			}

			for (Authority au : user.getRoles()) {
				grantedAuths.add(new SimpleGrantedAuthority(au.getRole()));
			}

			auth = new UsernamePasswordAuthenticationToken(user.getName(),
					user.getPassword(), grantedAuths);
			return auth;

		} catch (AuthenticationException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new BadCredentialsException("Error in authenticate", ex);
		}

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
