package br.com.projeto.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import br.com.project.entities.Authority;
import br.com.project.entities.User;
import br.com.project.service.LoginBO;

@Controller("userController")
public class UserController {
	
	@Resource
	LoginBO loginBO;
	
	//private List<Authority> authority;
	private User user;
	private String selectedRoles[];
	
	public String[] getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(String[] selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	@PreAuthorize("hasRole('ROLE_ADM')")
	public void addNewUser(){
		System.out.println("UserController::addNewUser");
		this.loginBO.addUser(this.user, selectedRoles);
		FacesMessage message = new FacesMessage("Succeful ", this.user.getName() + " added.");
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize(); 
		System.out.println("UserController destru�do");
	}
	
	public UserController(){
		System.out.println("UserController criado");
		this.user = new User();
	}
	
	@PreAuthorize("hasRole('ROLE_ADM')")
	public List<Authority> getAuthorities(){
		return this.loginBO.getAuthorities();
	}

	
	public List<User> getUsers(){
		return this.loginBO.getOtherUsers();
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
