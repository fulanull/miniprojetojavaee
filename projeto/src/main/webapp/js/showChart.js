function createGraphicLine(data, titleTop) {
	var titleBottom = "";
	generateChart(titleTop, "", titleBottom, data);
}

function generateChart(titleTop, titleLeft, titleBottom, dataGraphic) {
	var options = {
		chart : {
			renderTo : "container",
			defaultSeriesType : 'line',
			zoomType : 'xy'
		},
		title : {
			text : titleTop
		},
		credits : {
			enabled : false
		},
		xAxis : {
			type : 'datetime',
			title : {
				text : titleBottom
			},
			labels : {
				enabled : true
			}
		},
		yAxis : {
			title : {
				text : titleLeft
			},
			labels : {
				formatter : function() {
					return this.value + ' ';
				}
			}
		},
		tooltip : {
			formatter : function() {
				return ''
						+ Highcharts.dateFormat('%d-%m-%Y %H:%M:%S', this.x,
								true) + ' <br/>' + this.series.name + ': '
						+ '<b>' + Highcharts.numberFormat(this.y, 2) + '</b>';
			}
		},
		plotOptions : {
			series : {
				dataLabels : {
					enabled : false,
					x : -3,
					y : 0,
					formatter : function() {
						return Highcharts.numberFormat(this.y, 2);
					}
				},
				enableMouseTracking : true,
				cursor : 'pointer'
			}
		},
		series : []
	};

	
	var xValues = {
		data : []
	};

	var lines = dataGraphic.split(';');
	$.each(lines, function(lineNo, line) {
		var items = line.split(',');
		if (lineNo == 0) {
			$.each(items, function(itemNo, item) {
				if (itemNo > 0) {
					xValues.data.push(item);
				}
			});
		} else {

			var series = {
				data : []
			};

			$.each(items, function(itemNo, item) {

				var point = [];
				if (itemNo == 0) {
					series.name = item;
					series.type = 'line';

				} else {
					var xValue = xValues.data[itemNo - 1];

					point.push(parseFloat(xValue));
					point.push(parseFloat(item));

					series.data.push(point);
				}
			});
			options.series.push(series);
		}
	});

	Highcharts.setOptions({
		global : {
			useUTC : false
		}
	});
	chart = new Highcharts.Chart(options);

}

function updateCompareHistory(dataGraphic, options) {
	// Split the lines

}