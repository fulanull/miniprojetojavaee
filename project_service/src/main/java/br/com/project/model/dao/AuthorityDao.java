package br.com.project.model.dao;

import java.util.List;

import br.com.project.entities.Authority;

public interface AuthorityDao {
	
	public Authority addAuthority(Authority authority);
	
	public Authority getByRole(String roleName);
	
	public Authority getById(long id);
	
	public List<Authority> getAll();

}
