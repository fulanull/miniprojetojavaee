package br.com.project.model.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.project.entities.User;
import br.com.project.model.dao.DbSession;
import br.com.project.model.dao.UserDao;

@Repository("UserDao")
public class UserDAOImpl implements UserDao {

	private DbSession session;

	public UserDAOImpl() {
		this.session = new DbSessionHibernate();
	}

	@Override
	public User getById(long id) {
		// TODO Auto-generated method stub
		EntityManager db = this.session.getEntityManager();
		User us = db.getReference(User.class, id);
		db.close();

		return us;
	}

	@Override
	public User addUser(User user) {
		EntityManager db = this.session.getEntityManager();
		EntityTransaction tx = db.getTransaction();
		tx.begin();
		db.persist(user);
		tx.commit();
		db.close();
		return user;
	}

	@Override
	public User getByNameAndPassword(String username, String password)
			throws Exception {
		// TODO Auto-generated method stub
		EntityManager db = this.session.getEntityManager();
		TypedQuery<User> q = db.createQuery(
				"select a from User a where a.name = ?1 and a.password = ?2 ",
				User.class);
		q.setParameter(1, username);
		q.setParameter(2, password);
		User us = null;
		try {
			us = q.getSingleResult();
		} catch (NoResultException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		} finally {
			db.close();
		}

		return us;
	}

	@Override
	public User getByUsername(String username) {
		EntityManager db = this.session.getEntityManager();
		TypedQuery<User> q = db.createQuery(
				"select a from User a where a.name = ?1", User.class);
		q.setParameter(1, username);
		User us = null;
		try {
			us = q.getSingleResult();
		} catch (NoResultException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		} finally {
			db.close();
		}

		return us;
	}

	@Override
	public List<User> getAll() {
		EntityManager db = this.session.getEntityManager();
		List<User> list = db.createQuery("select a from User a", User.class)
				.getResultList();
		db.close();
		return list;
	}

	@Override
	public List<User> getAll(User ignored) {
		EntityManager db = this.session.getEntityManager();
		List<User> list ;
		TypedQuery<User> q = db.createQuery(
				"select a from User a where a.name <> ?1", User.class);
		q.setParameter(1, ignored.getName());

		try {
			list = q.getResultList();
		} catch (NoResultException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		} finally {
			db.close();
		}

		return list;
	}

}
