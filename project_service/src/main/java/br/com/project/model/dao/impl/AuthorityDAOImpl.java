package br.com.project.model.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.project.entities.Authority;
import br.com.project.model.dao.AuthorityDao;
import br.com.project.model.dao.DbSession;

@Repository("AuthorityDao")
public class AuthorityDAOImpl implements AuthorityDao {

	private DbSession session;

	public AuthorityDAOImpl() {
		this.session = new DbSessionHibernate();
	}

	@Override
	public Authority addAuthority(Authority authority) {
		EntityManager db = this.session.getEntityManager();
		EntityTransaction tx = db.getTransaction();
		tx.begin();
		db.persist(authority);
		tx.commit();
		db.close();

		return authority;
	}

	@Override
	public Authority getByRole(String role) {
		Authority authority;
		EntityManager db = this.session.getEntityManager();
		TypedQuery<Authority> q = db.createQuery("select a from Authority a where a.role = ?1", Authority.class);
		q.setParameter(1, role);

		try {
			authority = q.getSingleResult();
		} catch (NoResultException ex) {
			System.out.println(ex.getMessage());
			authority = null;
		} finally {
			db.close();
		}

		return authority;
	}

	@Override
	public Authority getById(long id) {
		Authority authority;
		EntityManager db = this.session.getEntityManager();
		authority = db.getReference(Authority.class, id);
		db.close();
		return authority;
	}

	@Override
	public List<Authority> getAll() {
		EntityManager db = this.session.getEntityManager();
		List<Authority> list = db.createQuery("select a from Authority a", Authority.class).getResultList();
		db.close();
		return list;
	}

}
