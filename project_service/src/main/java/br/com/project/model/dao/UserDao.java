package br.com.project.model.dao;

import java.util.List;

import br.com.project.entities.User;

public interface UserDao {
	
	public User getById(long id);
	
	public User getByUsername(String username);

	public User addUser(User user);
		
	public User getByNameAndPassword(String username, String password)   throws Exception;
	
	public List<User> getAll();
	
	public List<User> getAll(User ignored);

}
