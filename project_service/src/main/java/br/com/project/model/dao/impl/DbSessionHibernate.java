package br.com.project.model.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.project.model.dao.DbSession;

public class DbSessionHibernate implements DbSession {

	private static EntityManagerFactory entityManager = null;
	
	public DbSessionHibernate(){
		if(DbSessionHibernate.entityManager == null){
			DbSessionHibernate.entityManager = Persistence.createEntityManagerFactory("igor");
		}
	}
	

	@Override
	public EntityManager getEntityManager() {
		return DbSessionHibernate.entityManager.createEntityManager();
	}

}
