package br.com.project.model.dao;

import javax.persistence.EntityManager;


public interface DbSession {
	public EntityManager getEntityManager();
}
