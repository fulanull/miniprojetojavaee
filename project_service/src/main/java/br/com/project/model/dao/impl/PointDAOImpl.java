package br.com.project.model.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.project.entities.Point;
import br.com.project.model.dao.DbSession;
import br.com.project.model.dao.PointDAO;

@Repository("PointDAO")
public class PointDAOImpl implements PointDAO {

	private DbSession session;

	public PointDAOImpl() {
		this.session = new DbSessionHibernate();

	}

	@Override
	public Point getById(long id) {
		// TODO Auto-generated method stub
		EntityManager db = this.session.getEntityManager();
		Point point = db.getReference(Point.class, id);
		db.close();

		return point;
	}

	@Override
	public void delete(Point point) {
		
		EntityManager db = this.session.getEntityManager();
		
		Point p = db.getReference(Point.class, point.getId());
		
		EntityTransaction tx = db.getTransaction();
		tx.begin();
		
		db.remove(p);
		
		tx.commit();
		db.close();
		
	}

	@Override
	public List<Point> getAll() {
		// TODO Auto-generated method stub
		EntityManager db = this.session.getEntityManager();
		List<Point> list = db.createQuery("select a from Point a", Point.class).getResultList();
		db.close();
		return list;
	}

	@Override
	public void add(Point point) {
		// TODO Auto-generated method stub
		EntityManager db = this.session.getEntityManager();
		EntityTransaction tx = db.getTransaction();
		tx.begin();
		db.persist(point);
		tx.commit();
		db.close();
	}

	@Override
	public void add(List<Point> points) {
		EntityManager db = this.session.getEntityManager();
		EntityTransaction tx = db.getTransaction();
		tx.begin();

		for (Point point : points) {
			db.persist(point);
		}
		tx.commit();
		db.close();

	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	public void deleteAll() {
		EntityManager db = this.session.getEntityManager();
		EntityTransaction tx = db.getTransaction();
		tx.begin();

		db.createQuery("delete from Point").executeUpdate();

		tx.commit();
		db.close();
	}

	@Override
	public List<String> getTitles() {
		EntityManager db = this.session.getEntityManager();
		db.createQuery("from Point", Point.class);
		TypedQuery<String> q = db.createQuery("select distinct a.title from Point a order by a.title ASC", String.class);
		
		List<String> list = q.getResultList();
		db.close();
		return list;
	}

	@Override
	public List<Point> getByTitle(String title) {

		EntityManager db = this.session.getEntityManager();
		db.createQuery("from Point", Point.class);
		TypedQuery<Point> q = db.createQuery("select a from Point a where a.title = ?1", Point.class);
		q.setParameter(1, title);
		List<Point> list = q.getResultList();
		db.close();
		return list;
	}

}
