package br.com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import br.com.project.entities.Point;
import br.com.project.model.dao.PointDAO;
import br.com.project.service.PointBO;

@Service("PointBO")
@PreAuthorize("hasRole('ROLE_USR')")
public class PointBOImpl implements PointBO{
	@Resource
	private PointDAO pointDao;
	
	@Override
	public Point getById(long id) {
		return this.pointDao.getById(id);
	}

	@Override
	public void delete(Point point) {
		this.pointDao.delete(point);		
	}

	@Override
	public void deleteAll() {
		this.pointDao.deleteAll();		
	}

	@Override
	public List<Point> getAll() {
		return this.pointDao.getAll();
	}

	@Override
	public void add(Point point) {
		this.pointDao.add(point);		
	}

	@Override
	public void add(List<Point> points) {
		this.pointDao.add(points);		
	}

	@Override
	public List<String> getTitles() {
		return this.pointDao.getTitles();
	}

	@Override
	public List<Point> getByTitle(String title) {
		return this.pointDao.getByTitle(title);
	}

	@Override
	public String getDataByTitle(String title) {
		List<Point> points = this.getByTitle(title);
		
		if (points != null) {
//			DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			StringBuilder sbX = new StringBuilder("datas");
			StringBuilder sbI = new StringBuilder("Serie I");
			StringBuilder sbH = new StringBuilder("Serie H");
			StringBuilder sbHI = new StringBuilder("Serie (H + I)");
			
			for(Point p : points){
				
				sbX.append(", ");
				sbX.append(p.getX().getTime());
				//sbX.append( df.format( p.getX()) );
				
				sbI.append(", ");
				sbI.append(p.getI() );
				
				sbH.append(", ");
				sbH.append(p.getH() );
				
				sbHI.append(", ");
				sbHI.append(p.getI() + p.getH() );				
			}
			
			sbX.append("; ");
			sbH.append("; ");
			sbI.append("; ");
			
			sbX.append(sbH);
			sbX.append(sbI);
			sbX.append(sbHI);
			
			return sbX.toString();
		}
		return null;
	}
	
}
