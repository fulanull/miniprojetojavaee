package br.com.project.service.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import br.com.project.entities.Point;

public class ParserXLSM {

	public static List<Point> getPoints(InputStream dados) throws IOException{
		XSSFWorkbook aux = new XSSFWorkbook(dados);
		XSSFSheet sheet = aux.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.iterator();
		List<Point> list = new ArrayList<Point>();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			list.add( ParserXLSM.getPointFromRow(row) );
		}
		
		return list;
	}
	
	private static Point getPointFromRow(Row row) {
		Point point = new Point();

		StringBuilder title = new StringBuilder(row.getCell(0).toString());
		title.append(" ");
		title.append(row.getCell(1).toString());
		title.append(" ");
		title.append(row.getCell(2).toString());

		point.setTitle(title.toString());
		point.setX(row.getCell(3).getDateCellValue());
		point.setH(Double.parseDouble(row.getCell(7).toString()));
		point.setI(Double.parseDouble(row.getCell(8).toString()));

		return point;
	}
}
