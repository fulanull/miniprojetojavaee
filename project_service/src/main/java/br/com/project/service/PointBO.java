package br.com.project.service;

import java.util.List;

import br.com.project.entities.Point;

public interface PointBO {
	
	public Point getById(long id);

	public void delete(Point point);

	public void deleteAll();

	public List<Point> getAll();

	public void add(Point point);

	public void add(List<Point> points);
	
	public List<String> getTitles();
	
	public List<Point> getByTitle(String title);
	
	public String getDataByTitle(String title);

}
