package br.com.project.service;

import java.util.List;

import br.com.project.entities.Authority;
import br.com.project.entities.User;

public interface LoginBO {
	public User getById(long id);

	public User addUser(User user, String roles[]);	
	
	public User getLogedUser();
	
	public List<Authority> getAuthorities();
	
	public User login(String username, String password)
			throws Exception;
	
	public List<User> getOtherUsers();
}
