package br.com.project.service;

import java.io.IOException;
import java.io.InputStream;

public interface FileUploadBO {
	public void addData(InputStream dados) throws IOException;
}
