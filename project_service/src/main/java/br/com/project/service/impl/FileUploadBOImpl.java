package br.com.project.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.project.entities.Point;
import br.com.project.model.dao.PointDAO;
import br.com.project.model.dao.impl.PointDAOImpl;
import br.com.project.service.FileUploadBO;
import br.com.project.service.parser.ParserXLSM;

@Service("fileUpload")
public class FileUploadBOImpl implements FileUploadBO {

	public FileUploadBOImpl() {
		System.out.println("FileUploadImpl : constru�do");
	}

	@Override
	public void addData(InputStream dados) throws IOException {

		System.out.println("FileUploadImpl : addDados");
		
		List<Point> list = ParserXLSM.getPoints(dados);
		PointDAO dao = new PointDAOImpl();
		dao.deleteAll();
		dao.add(list);

	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
		System.err.println("FileUploadImpl: Destru�do.");
	}

}
