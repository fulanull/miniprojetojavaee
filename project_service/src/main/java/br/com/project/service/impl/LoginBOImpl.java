package br.com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.project.entities.Authority;
import br.com.project.entities.User;
import br.com.project.model.dao.AuthorityDao;
import br.com.project.model.dao.UserDao;
import br.com.project.service.LoginBO;

@Service("loginBO")
public class LoginBOImpl implements LoginBO {

	@Resource
	UserDao userDao;
	
	@Resource
	AuthorityDao authorityDao;
	
	@Override
	@PreAuthorize("hasRole('ROLE_USR')")
	public User getById(long id) {
		return this.userDao.getById(id);
	}

	@Override
	@PreAuthorize("hasRole('ROLE_USR')")
	public User addUser(User user, String roles[]) {
		Authority aux;
		for(String role : roles){
			aux = this.authorityDao.getByRole(role);
			user.getRoles().add(aux);
		}
		return this.userDao.addUser(user);
	}

	@Override
	public User login(String username, String password) throws Exception {
		return this.userDao.getByNameAndPassword(username, password);
	}

	@Override
	@PreAuthorize("hasRole('ROLE_USR')")
	public User getLogedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User us = this.userDao.getByUsername(auth.getName());

		return us;
	}

	@Override
	public List<Authority> getAuthorities() {
		return this.authorityDao.getAll();
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADM')")
	public List<User> getOtherUsers() {		
		return this.userDao.getAll( this.getLogedUser());
	}

}
